# Name: Sri Padala
# WSU ID: U424P963
from queue import PriorityQueue

from homework_ai.cost import *
from homework_ai.children import *


def a_star_algorithm(initial_state, final_goal_state):
    curr_node = deepcopy(initial_state)
    frontier = PriorityQueue()
    f_cost = find_total_path_cost(curr_node, final_goal_state)
    frontier.put((f_cost, curr_node, None))
    explored_states = list()
    while True:
        if len(frontier.queue) == 0:
            raise Exception("Frontier Empty")
            break
        curr_path_cost, curr_node, parent_direction = frontier.get()
        if curr_node == final_goal_state:
            explored_states.append((curr_node, parent_direction))
            print("Search Completed")
            break
        explored_states.append((curr_node, parent_direction))

        for child, direct in find_the_children(curr_node):
            g_cost = find_total_path_cost(initial_state, child)
            h_cost = find_total_path_cost(child, final_goal_state)
            path_cost = int(g_cost + h_cost)
            if (not check_explored_states(child, explored_states)) and (not check_frontier_states(child, frontier)):
                frontier.put((path_cost, child, direct))
            elif (check_frontier_states(child, frontier)) and (path_cost < get_key_from_value(frontier, child)):
                frontierList = frontier.queue
                frontierList.remove((get_key_from_value(frontier, child), child, get_direction_from_node(frontier, child)))
                frontier.put((path_cost, child, direct))

    solution = print_path(explored_states)

    solution.reverse()

    for num, s in enumerate(solution):
        print()
        print()
        print("step: {}".format(num))
        print_matrix(s)


if __name__ == "__main__":
    start_state, goal_state = ask_user_for_input()
    print("\n______________________________________Game Start_____________________________________________")
    a_star_algorithm(start_state, goal_state)
