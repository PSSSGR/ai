# Name: Sri Padala
# WSU ID: U424P963
from homework_ai.util_functions import *


def swap(child_matrix, initial_row, initial_col, final_row, final_col):
    """
    Function to move the blank slot given the direction.
    Args:
        child_matrix:
        initial_row:
        initial_col:
        final_row:
        final_col:

    Returns:

    """
    temp = child_matrix[initial_row][initial_col]
    child_matrix[initial_row][initial_col] = child_matrix[final_row][final_col]
    child_matrix[final_row][final_col] = temp


def move_the_matrix(current_state, move: str):
    """
    Function to return a new child matrix from a parent matrix given a move.
    Args:
        current_state:
        move:

    Returns: child matrix

    """
    child_matrix = deepcopy(current_state)
    blank_location = find_the_location(current_state, 0)
    row = blank_location["row"]
    column = blank_location["column"]
    if move == "Up":
        swap(child_matrix, row, column, row + 1, column)
    elif move == "Down":
        swap(child_matrix, row, column, row - 1, column)
    elif move == "Left":
        swap(child_matrix, row, column, row, column - 1)
    elif move == "Right":
        swap(child_matrix, row, column, row, column + 1)

    return child_matrix


def find_the_children(current_state):
    """
    Function to find the children of the given the state configuration.
    Args:
        current_state:

    Returns: List containing the children states and the particular move.

    """
    blank_location = find_the_location(current_state, 0)
    children_states = []
    # Move up
    if blank_location["row"] + 1 < len(current_state):
        possible_child_state = move_the_matrix(current_state, "Up")
        children_states.append((possible_child_state, "Up"))
    # Move Down
    if len(current_state) > blank_location["row"] - 1 >= 0:
        possible_child_state = move_the_matrix(current_state, "Down")
        children_states.append((possible_child_state, "Down"))
    # Move Right
    if blank_location["column"] + 1 < len(current_state):
        possible_child_state = move_the_matrix(current_state, "Right")
        children_states.append((possible_child_state, "Right"))
    # Move Left
    if len(current_state) > blank_location["column"] - 1 >= 0:
        possible_child_state = move_the_matrix(current_state, "Left")
        children_states.append((possible_child_state, "Left"))

    return children_states
