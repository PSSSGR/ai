# Name: Sri Padala
# WSU ID: U424P963
def find_total_path_cost(curr_state, next_state):
    """
    Function to calculate the path cost of the current state given the next state or goal state.
    Args:
        curr_state:
        next_state:
        till_cost: g(n)

    Returns: Total path cost f(n) = g(n) + h(n)
    """
    total_path_cost = 0
    for row in reversed(list(range(3))):
        for col in list(range(3)):
            if curr_state[row][col] != 0:
                elem_cost = find_manhattan_distance(curr_state[row][col], next_state, row, col)
                total_path_cost += elem_cost

    return total_path_cost


def find_manhattan_distance(elem, next_state, row, col):
    """
    Function to fin the Manhattan distance.
    Args:
        elem: current element
        next_state:
        row: row count in the current state.
        col: col count in the current state.

    Returns: Manhattan distance between the given element and the next state.

    """
    for row_count in reversed(list(range(3))):
        for col_count in list(range(3)):
            if elem == next_state[row_count][col_count]:
                return abs(row - row_count) + abs(col - col_count)
