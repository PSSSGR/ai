# Name: Sri Padala
# WSU ID: U424P963
from copy import deepcopy


def print_matrix(matrix):
    """
    Function to print the 2-D matrix.
    Args:
        matrix: Contains the current state.

    Returns: -

    """
    for row in reversed(list(range(3))):
        print()
        for col in list(range(3)):
            print(matrix[row][col], end=" ")


def ask_user_for_input():
    """
    Function to ask the user input for start state and goal state.
    Returns: tuple containing the start and goal states.

    """
    start_matrix = []
    for row in reversed(list(range(3))):
        row_container = []
        for col in list(range(3)):
            row_container.append(int(input("Enter value for the tile located at ({}, {}) coordinate position for start configuration :".format(row, col))))
        start_matrix.insert(0, row_container)

    goal_matrix = []
    for row in reversed(list(range(3))):
        row_container = []
        for col in list(range(3)):
            row_container.append(int(input("Enter value for the tile located at ({}, {}) coordinate position for goal configuration :".format(row, col))))
        goal_matrix.insert(0, row_container)

    return start_matrix, goal_matrix


def find_the_location(matrix, elem):
    """
    Funtion to return the location of an element in the matrix.
    Args:
        matrix:
        elem:

    Returns: Location

    """
    for row_count in reversed(list(range(3))):
        for col_count in list(range(3)):
            if elem == matrix[row_count][col_count]:
                return {"row": row_count, "column": col_count}





def get_least(frontier_list):
    """

    Args:
        frontier_list:

    Returns:

    """
    keys = [key[0] for key in frontier_list]
    return min(keys)


def check_explored_states(child_state, given_states):
    for state, _ in given_states:
        if child_state == state:
            return True
    else:
        return False


def check_frontier_states(child_state, given_states):
    for cost, state, _ in given_states.queue:
        if child_state == state:
            return True
    else:
        return False


def get_key_from_value(frontier_list, elem):
    for cost, state, _ in frontier_list.queue:
        if elem == state:
            return cost
    else:
        raise Exception("No element is present")


def get_direction_from_node(frontier_list, elem):
    for _, state, direct in frontier_list.queue:
        if elem == state:
            return direct
    else:
        raise Exception("No element is present")


def get_explored_from_node(explored_list, elem):
    for node, direction in explored_list:
        if elem == node:
            return direction
    else:
        raise Exception("No element is present")


def print_path(states):
    start_node = states[0][0]
    goal_state = states[len(states)-1][0]
    node = deepcopy(goal_state)
    solution_path = list()
    solution_path.append(goal_state)
    while node != start_node:
        direction = get_explored_from_node(states, node)
        if direction == "Right":
            node = move_the_matrix(node, "Left")
        elif direction == "Left":
            node = move_the_matrix(node, "Right")
        elif direction == "Down":
            node = move_the_matrix(node, "Up")
        elif direction == "Up":
            node = move_the_matrix(node, "Down")
        solution_path.append(node)

    return solution_path


def move_the_matrix(current_state, move: str):
    """
    Function to return a new child matrix from a parent matrix given a move.
    Args:
        current_state:
        move:

    Returns: child matrix

    """
    child_matrix = deepcopy(current_state)
    blank_location = find_the_location(current_state, 0)
    row = blank_location["row"]
    column = blank_location["column"]
    if move == "Up":
        swap(child_matrix, row, column, row + 1, column)
    elif move == "Down":
        swap(child_matrix, row, column, row - 1, column)
    elif move == "Left":
        swap(child_matrix, row, column, row, column - 1)
    elif move == "Right":
        swap(child_matrix, row, column, row, column + 1)

    return child_matrix


def swap(child_matrix, initial_row, initial_col, final_row, final_col):
    """
    Function to move the blank slot given the direction.
    Args:
        child_matrix:
        initial_row:
        initial_col:
        final_row:
        final_col:

    Returns:

    """
    temp = child_matrix[initial_row][initial_col]
    child_matrix[initial_row][initial_col] = child_matrix[final_row][final_col]
    child_matrix[final_row][final_col] = temp
